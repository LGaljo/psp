package si.unilj.fri.pbd.lab3;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class ProfileActivity extends AppCompatActivity {
    Button cameraButton;
    Button profShowMessage;
    EditText fullNameBox;
    EditText fullName;
    TextView nameBox;
    SharedPreferences sharedPreferences;
    final int REQUEST_IMAGE_CAPTURE = 0;
    final static public String EXTRA_MESSAGE = "MyApp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        cameraButton = (Button)findViewById(R.id.prof_button_camera);
        profShowMessage = (Button)findViewById(R.id.profile_show_message);
        fullNameBox = findViewById(R.id.edit_name);
        fullName = findViewById(R.id.prof_msg_text);
        nameBox = findViewById(R.id.prof_name);

        sharedPreferences = getSharedPreferences("preferences", MODE_PRIVATE);
        String name = sharedPreferences.getString("full_name", "");
        nameBox.setText(name);

        cameraButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });

        profShowMessage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                Intent intent = new Intent(ProfileActivity.this,
                        DisplayMessageActivity.class);
                EditText editText = (EditText) findViewById(R.id.prof_msg_text);
                String message = editText.getText().toString();
                intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            ImageView profileView = (ImageView) findViewById(R.id.prof_image);
            profileView.setImageBitmap(imageBitmap);
        }
    }
}
