package si.unilj.fri.pbd.lab3;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class RegistrationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
    }

    public void registerUser(View view) {
        EditText username = findViewById(R.id.edit_name);
        if (username.length() == 0) {
            username.setError(getString(R.string.error_username));
            return;
        }

        SharedPreferences sharedPreferences = getSharedPreferences("preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("full_name", username.getText().toString());
        editor.apply();

        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
}
