package si.uni_lj.fri.lrk.lab7;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import si.uni_lj.fri.lrss.machinelearningtoolkit.MachineLearningManager;
import si.uni_lj.fri.lrss.machinelearningtoolkit.classifier.Classifier;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Instance;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.MLException;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Value;

public class TrainClassifierService extends IntentService {
    private static String TAG = TrainClassifierService.class.getSimpleName();

    public TrainClassifierService() {
        super("TrainClassifierService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent == null) return;

        double mean = intent.getFloatExtra("mean", -1);
        double var = intent.getFloatExtra("variance", -1);
        double mcr = intent.getFloatExtra("mcr", -1);
        String label = intent.getStringExtra("name");

        Log.d(TAG, "onHandleIntent: ");

        try {
            ArrayList<Value> instanceValues = new ArrayList<>();
            instanceValues.add(new Value(mean, Value.NUMERIC_VALUE));
            instanceValues.add(new Value(mcr, Value.NUMERIC_VALUE));
            instanceValues.add(new Value(var, Value.NUMERIC_VALUE));
            instanceValues.add(new Value(label, Value.NOMINAL_VALUE));
            Instance instance = new Instance(instanceValues);
            ArrayList<Instance> instances = new ArrayList<>();
            instances.add(instance);
            MachineLearningManager mManager = MachineLearningManager.getMLManager(ApplicationContext.getContext());
            Classifier c = mManager.getClassifier("movementClassifier");
            c.train(instances);
            c.printClassifierInfo();
        } catch (MLException e) {
            e.printStackTrace();
        }
    }
}
