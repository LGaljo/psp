package si.uni_lj.fri.lrk.lab7;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;

import java.util.ArrayList;

import si.uni_lj.fri.lrss.machinelearningtoolkit.MachineLearningManager;
import si.uni_lj.fri.lrss.machinelearningtoolkit.classifier.Classifier;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.ClassifierConfig;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Constants;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Feature;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.FeatureNominal;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.FeatureNumeric;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Instance;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.MLException;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Signature;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Value;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    private static Handler mHandler = new Handler();

    AccBroadcastReceiver mBcastRecv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBcastRecv = new AccBroadcastReceiver();

        Button startButton = (Button) findViewById(R.id.start_btn);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandler.postDelayed(r, 5000);
            }
        });

        Button stopButton = (Button) findViewById(R.id.stop_btn);
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandler.removeCallbacks(r);
            }
        });


        try {
            MachineLearningManager mManager = MachineLearningManager.getMLManager(ApplicationContext.getContext());
            Feature accMean = new FeatureNumeric("accMean");
            Feature varMean = new FeatureNumeric("varMean");
            Feature mcrMean = new FeatureNumeric("mcrMean");
            ArrayList<String> classValues = new ArrayList<>();
            classValues.add("Horizontal");
            classValues.add("Vertical");
            classValues.add("Still");
            Feature movement = new FeatureNominal("movement", classValues);
            ArrayList<Feature> features = new ArrayList<>();
            features.add(accMean);
            features.add(mcrMean);
            features.add(varMean);
            features.add(movement);
            Signature signature = new Signature(features, features.size() - 1);
            mManager.addClassifier(Constants.TYPE_NAIVE_BAYES, signature, new ClassifierConfig(), "movementClassifier");
        } catch (MLException e) {
            e.printStackTrace();
        }
    }

    final Runnable r = () -> {
        Log.d(TAG, "start service");
        Intent intent = new Intent(ApplicationContext.getContext(), AccSenseService.class);
        startService(intent);
        mHandler.postDelayed(this.r, 5000);
    };

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mBcastRecv,
                new IntentFilter("si.uni_lj.fri.lrk.lab7.BROADCAST_RESULT"));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBcastRecv);
        super.onPause();
        mHandler.removeCallbacks(r);
    }

    public void recordAccData(float intensityf, float variancef, float MCRf) {
        // Handle the data here
        Log.d(TAG, "recordAccData Intensity: " + intensityf + " var " + variancef + " MCR " + MCRf);
        Switch training = (Switch)findViewById(R.id.switch_train);
        RadioGroup typeOfClassifier = (RadioGroup)findViewById(R.id.radioGroup);
        String name = "";

        switch (typeOfClassifier.getCheckedRadioButtonId()) {
            case R.id.radioButton1:
                name = getResources().getString(R.string.still);
                break;
            case R.id.radioButton2:
                name = getResources().getString(R.string.vertical);
                break;
            case R.id.radioButton3:
                name = getResources().getString(R.string.horizontal);
                break;
        }

        if (training.isChecked()) {
            Intent intent = new Intent(this, TrainClassifierService.class);
            intent.putExtra("mean", intensityf);
            intent.putExtra("variance", variancef);
            intent.putExtra("mcr", MCRf);
            intent.putExtra("name", name);
            startService(intent);
        } else {
            try {
                ArrayList<Value> instanceValues = new ArrayList<>();
                instanceValues.add(new Value((double)intensityf, Value.NUMERIC_VALUE));
                instanceValues.add(new Value((double)MCRf, Value.NUMERIC_VALUE));
                instanceValues.add(new Value((double)variancef, Value.NUMERIC_VALUE));
                Instance instance = new Instance(instanceValues);
                MachineLearningManager mManager = MachineLearningManager.getMLManager(ApplicationContext.getContext());
                Classifier c = mManager.getClassifier("movementClassifier");
                Value inference = c.classify(instance);
                Log.d(TAG, "Activnost: " + inference.getValue().toString());
            } catch (MLException e) {
                e.printStackTrace();
            }
        }
    }

    public class AccBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            float intensity = intent.getFloatExtra("mean", 0);
            float variance = intent.getFloatExtra("variance", 0);
            float MCR = intent.getFloatExtra("MCR", 0);

            recordAccData(intensity, variance, MCR);
        }
    }
}