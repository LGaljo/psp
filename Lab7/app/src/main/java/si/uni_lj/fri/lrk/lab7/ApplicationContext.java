package si.uni_lj.fri.lrk.lab7;

import android.app.Application;
import android.content.Context;

public class ApplicationContext extends Application {

    private static Context mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = getApplicationContext();
    }

    public static Context getContext()
    {
        return mInstance;
    }

}